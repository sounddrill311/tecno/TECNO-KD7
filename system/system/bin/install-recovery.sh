#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:6dec01fbfe75bb0fcc59abc048c36cf2dbdd1cfd; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:60f0f86d8c44515609d923ecc060c08e186b4602 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:6dec01fbfe75bb0fcc59abc048c36cf2dbdd1cfd && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
